#!/usr/local/bin/python3
# https://github.com/pyotp/pyotp

import argparse, pyotp, sys, time

class ArgsKeep:
	def __init__(self):
		parser = argparse.ArgumentParser(
			description="MFA OTP Generator",
			epilog="usage: mfa_otpgen.py --key [secret key]"
			)
		parser.add_argument('--key', required=True, help="provide secret key value")
		self.args=parser.parse_args()

def generate_otp():
	param = ArgsKeep().args
	totp = pyotp.TOTP(param.key, interval=30)
	print("Your OTP (valid 30s, refreshed every 5s)\n[press Ctrl+c to stop]")
	try:
		while True:
			print(totp.now())
			time.sleep(5)
	except KeyboardInterrupt:
		pass

if __name__ == "__main__":
	try:
		generate_otp()
	except Exception as E:
		print("There was an error:\n", E)

