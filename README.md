# MFA OTP Generator #

This script is used to generate MFA One Time Passwords based on provided security key.
It generates 6 digit OTP's that are valid for 30 seconds (Similar to Google Authenticator).

### Usage: ###

python3 mfa_otpgen.py --key XXXGMVD7VKNDEHLVG3AJ6LFTKCSQZFIISBXXXH3MCBCPHB37SWFIPBL4QSV5HXXX 

### Prerequisites: ###
* python3
* python pyotp module ( https://github.com/pyotp/pyotp )
